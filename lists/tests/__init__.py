def docstring_decorator(method):
    """
    This decorator allows me to print out the method docstring when a test ecounters and error.
    The set up must be as follows:

    def test_something_amazing(self, me):
        self.fail(me.__doc__)

    The docstring can be accessed via the name of the first argument .__doc__
    """
    def wrapper(self, *args, **kwargs):
        return method(self, me=method, *args, **kwargs)
    return wrapper

from django.test import TestCase
from django.core.exceptions import ValidationError

from lists.models import Item, List

from . import docstring_decorator

class ListAndItemModelTest(TestCase):        


    def test_saving_and_retrieving_items(self):
        list_ = List()
        list_.save()

        first_item = Item()
        first_item.text = 'The first list item ever!'
        first_item.list = list_
        first_item.save()

        second_item = Item()
        second_item.text = 'Item the second!'
        second_item.list = list_
        second_item.save()

        saved_lists = List.objects.all()
        self.assertEqual(saved_lists.count(), 1)
        self.assertEqual(saved_lists[0], list_)
        saved_items = Item.objects.all()
        self.assertEqual(saved_items.count(), 2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual('The first list item ever!', first_saved_item.text)
        self.assertEqual(first_saved_item.list, list_)
        self.assertEqual('Item the second!', second_saved_item.text)
        self.assertEqual(second_saved_item.list, list_)


    def test_cannot_save_empty_list_items(self):
        list1 = List.objects.create()
        item = Item(list=list1, text='')
        with self.assertRaises(ValidationError):
            item.save()
            

    def test_get_absolute_url(self):
        list1 = List.objects.create()
        self.assertEqual(list1.get_absolute_url(), '/lists/%d/' % (list1.id,))


    def test_cannot_save_duplicate_items(self):
        list1 = List.objects.create()
        Item.objects.create(list=list1, text='bla')
        with self.assertRaises(ValidationError):
            Item.objects.create(list=list1, text='bla')

    @docstring_decorator
    def test_CAN_save_same_item_to_different_lists(self, me):
        """
        We shouldn't raise an error when duplicate Items added to seperate Lists
        """
        list1 = List.objects.create()
        list2 = List.objects.create()
        try:
            Item.objects.create(list=list1, text='bla')
            Item.objects.create(list=list2, text='bla')
        except:
            self.fail(me.__doc__)

    @docstring_decorator
    def test_list_ordering(self, me):
        """
        Make sure that the order of Items in a List is based on the order in which they are added.
        """
        list1 = List.objects.create()
        items = []
        for text in ('i1', 'item 2', '3'):
            items.append(Item.objects.create(list=list1, text=text))
        self.assertEqual(
            list(Item.objects.all()),
            [item for item in items], msg=me.__doc__
        )

    @docstring_decorator
    def test_string_representation(self, me):
        """
        Make sure the string display value for an Item instance is the Item text.
        """
        list1 = List.objects.create()
        item1 = Item.objects.create(list=list1, text='some text')
        self.assertEqual(str(item1), item1.text, msg=me.__doc__)
    

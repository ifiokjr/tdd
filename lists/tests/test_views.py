from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.utils.html import escape

from lists.models import Item, List
from lists.forms import ItemForm, EMPTY_LIST_ERROR, DUPLICATE_ITEM_ERROR, ExistingListItemForm

from . import docstring_decorator


class HomePageTest(TestCase):
    maxDiff = None
    
    def test_home_page_renders_home_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'home.html',
                                msg_prefix="We should use the home template for root url '/'\n")


    def test_home_page_uses_item_form(self):
        response = self.client.get('/')
        self.assertIsInstance(response.context['form'], ItemForm,
                              msg="The form passed through to the root url should be an ItemForm.\n")



class NewListTest(TestCase):

    def test_saving_a_POST_request(self):
        self.client.post(
            '/lists/new',
            data={'text': 'A new list item'}
        )

        self.assertEqual(Item.objects.all().count(), 1)
        new_item = Item.objects.all()[0]
        self.assertEqual(new_item.text, 'A new list item')


    def test_redirects_after_POST(self):
        response = self.client.post(
            '/lists/new',
            data={'text': 'A new list item'}
        )

        self.assertEqual(response.status_code, 302)

        new_list = List.objects.all()[0]
        self.assertRedirects(response, '/lists/%d/' % (new_list.id,))


    @docstring_decorator
    def test_validation_errors_sent_back_to_home_page_template(self, me):
        """
        1: We shouldn't create a new Items for an empty string.
        2: We shouldn't create a new List for an empty string Item
        3: We should be direct back to the home template.
        4: Our response should include the 'EMPTY_LIST_ERROR' string
        5: There should be a ItemForm instance within the form response.
        """
        
        response = self.client.post('/lists/new', data={'text': ''})
        self.assertEqual(Item.objects.all().count(), 0,
                         msg="We shouldn't create any new Items for an empty string.\n")
        self.assertEqual(List.objects.all().count(), 0, msg=me.__doc__)
        self.assertContains(response, escape(EMPTY_LIST_ERROR))
        self.assertTemplateUsed(response, 'home.html',
                                msg_prefix='We should be directed back to the home page.\n')
        self.assertIsInstance(response.context['form'], ItemForm)

    @docstring_decorator
    def test_valid_input_does_not_produce_errors_on_page(self, me):
        """
        There shouldn't be any error on the redirect page after a valid has been added!
        """
        redirect = self.client.post('/lists/new', data={'text': 'valid input!'})
        response = self.client.get(redirect.url)
        self.assertEqual(Item.objects.all().count(), 1)
        self.assertNotContains(response, escape(EMPTY_LIST_ERROR))
        self.assertNotContains(response, escape(DUPLICATE_ITEM_ERROR))


class ListViewTest(TestCase):

    def test_uses_list_template(self):
        list_ = List.objects.create()
        response = self.client.get('/lists/%d/' % (list_.id,))
        self.assertTemplateUsed(response, 'list.html')


    def test_displays_only_items_for_that_list(self):
        correct_list = List.objects.create()
        Item.objects.create(text='itemey 1', list=correct_list)
        Item.objects.create(text='itemey 2', list=correct_list)
        other_list = List.objects.create()
        Item.objects.create(text='other list item 1', list=other_list)
        Item.objects.create(text='other list item 2', list=other_list)

        response = self.client.get('/lists/%d/' % (correct_list.id,))

        self.assertContains(response, 'itemey 1')
        self.assertContains(response, 'itemey 2')
        self.assertNotContains(response, 'other list item 1')
        self.assertNotContains(response, 'other list item 2')


    def test_passes_correct_list_to_template(self):
        other_list = List.objects.create()
        correct_list = List.objects.create()
        response = self.client.get('/lists/%d/' % (correct_list.id,))
        self.assertEqual(response.context['list'], correct_list)

    def test_can_save_a_POST_request_to_an_existing_list(self):
        other_list = List.objects.create()
        correct_list = List.objects.create()

        self.client.post(
            '/lists/%d/' % (correct_list.id,),
            data={'text': 'A new item for an existing list'}
        )

        self.assertEqual(Item.objects.all().count(), 1)
        new_item = Item.objects.all()[0]
        self.assertEqual(new_item.text, 'A new item for an existing list')
        self.assertEqual(new_item.list, correct_list)


    def test_POST_redirects_to_list_view(self):
        other_list = List.objects.create()
        correct_list = List.objects.create()

        response = self.client.post(
            '/lists/%d/' % (correct_list.id,),
            data={'text': 'A new item for an existing list'}
        )

        self.assertRedirects(response, '/lists/%d/' % (correct_list.id,))


    def post_invalid_input(self):
        list_ = List.objects.create()

        return self.client.post(
            '/lists/%d/' % (list_.id,),
            data={'text': ''}
        )


    @docstring_decorator
    def test_invalid_input_means_nothing_saved_to_db(self, me):
        """An empty string should not produce a new Item."""
        self.post_invalid_input()
        self.assertEqual(Item.objects.all().count(), 0, msg=me.__doc__)
        

    @docstring_decorator
    def test_invalid_input_renders_list_template(self, me):
        """We should redirect to current list page using 'list.html' teamplate"""
        response = self.post_invalid_input()
        self.assertTemplateUsed(response, 'list.html', msg_prefix=me.__doc__)

    @docstring_decorator
    def test_invalid_input_renders_form_with_errors(self, me):
        """
        The form that is passed to the response should be an ExistingListItemForm instance.
        The response should include the expected 'EMPTY_LIST_ERROR'
        """
        response = self.post_invalid_input()
        self.assertIsInstance(response.context['form'], ExistingListItemForm, msg=me.__doc__)
        self.assertContains(response, escape(EMPTY_LIST_ERROR), msg_prefix=me.__doc__)


    def test_duplicate_item_validation_errors_end_up_on_lists_page(self):
        """
        We should see errors passed through to the page and not a server error.
        """
        list1 = List.objects.create()
        item1 = Item.objects.create(list=list1, text="textey")
        response = self.client.post(
            '/lists/%d/' % (list1.id,),
            data={'text': 'textey'}
        )

        expected_error = escape(DUPLICATE_ITEM_ERROR)
        self.assertContains(response, expected_error)
        self.assertTemplateUsed(response, 'list.html')
        self.assertEqual(Item.objects.all().count(), 1)

    @docstring_decorator
    def test_displays_item_form(self, me):
        """
        Ensure that the ExistingListItemForm is always passed through to ListView response.
        """
        list_ = List.objects.create()
        response = self.client.get('/lists/%d/' % (list_.id,))
        self.assertIsInstance(response.context['form'], ExistingListItemForm, msg=me.__doc__)
        self.assertContains(response, 'name="text"', msg_prefix=me.__doc__)
        

    def test_valid_input_does_not_produce_errors_on_list_page(self):
        list1 = List.objects.create()
        item1 = Item.objects.create(list=list1, text="text item")
        initial = self.client.post(
            '/lists/%d/' % (list1.id,),
            data={'text': 'textey'}
        )

        response = self.client.get('/lists/%d/' % (list1.id,))
        print(response.context['form'])
        self.assertEqual(Item.objects.all().count(), 2)
        self.assertNotContains(response, escape(DUPLICATE_ITEM_ERROR))
        self.assertNotContains(response, escape(EMPTY_LIST_ERROR))

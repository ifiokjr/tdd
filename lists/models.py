from __future__ import unicode_literals

from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.shortcuts import resolve_url



@python_2_unicode_compatible
class List(models.Model):
    pass

    def get_absolute_url(self):
        return resolve_url('view_list', self.id)

@python_2_unicode_compatible
class Item(models.Model):
    text = models.TextField()
    list = models.ForeignKey(List)

    class Meta:
        ordering = ('id', )
        unique_together = ('list', 'text' )
    
    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.text

    def get_absolute_url(self):
        return self.list.get_absolute_url()

from django.conf.urls import patterns, include, url

from django.contrib import admin

from .views import NewListView, ViewAndAddToList

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^(?P<pk>\d+)/$', ViewAndAddToList.as_view(), name='view_list'),
    url(r'^new$', NewListView.as_view(), name='new_list'),
)

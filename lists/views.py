from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import FormView, CreateView
from django.views.generic.detail import SingleObjectMixin
from django.core.exceptions import ValidationError

from .models import Item, List
from .forms import ItemForm, ExistingListItemForm

class HomePageView(FormView):
    template_name = 'home.html'
    form_class = ItemForm

# def home_page(request):
#     return render(request, 'home.html', {'form': ItemForm()})



class ViewAndAddToList(CreateView):
    template_name = 'list.html'
    model = List
    form_class = ExistingListItemForm
    

    def get_form(self, form_class):
        self.object = self.get_object()
        return form_class(for_list=self.object, data=self.request.POST or None)
    
# def view_list(request, list_id):
#     list_ = List.objects.get(pk=list_id)
#     form = ExistingListItemForm(for_list=list_, data=request.POST or None)
#     if form.is_valid():
#         form.save()
#         return redirect(list_)
#     return render(request, 'list.html',{
#         'list': list_,
#         'form': form,
#     })


class NewListView(CreateView, HomePageView):
    """
    This view is the post destination for all items added from the home page
    It takes the posted form data, makes sure that it's valid and redirects to
    the relevant list id page.

    -- At the moment if a form is invalid the page doesn't change.
    """
    def form_valid(self, form):
        list_ = List.objects.create()
        form.save(for_list=list_)
        return redirect(list_)

# def new_list(request):
#     form = ItemForm(data=request.POST)
#     if form.is_valid():
#         list_ = List.objects.create()
#         form.save(for_list=list_)
#         return redirect(list_)
#     else:
#         return render(request, 'home.html', {'form': form})



    

Django==1.6
Werkzeug==0.9.4
django-extensions==1.2.5
ipython==1.1.0
selenium==2.37.2
six==1.4.1
gunicorn==18.0
South==0.8.4
